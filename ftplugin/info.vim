if &filetype == 'info'
	setlocal nolist
    nnoremap <buffer> <silent> <Plug>InfoHeadingNext :call info#HeadingNext()<CR>
    nnoremap <buffer> <silent> <Plug>InfoHeadingPrev :call info#HeadingPrev()<CR>
    nnoremap <buffer> <silent> <Plug>InfoNext :call info#GoRelative('Next')<CR>
    nnoremap <buffer> <silent> <Plug>InfoPrev :call info#GoRelative('Prev')<CR>
    nnoremap <buffer> <silent> <Plug>InfoUp :call info#GoRelative('Up')<CR>
    nnoremap <buffer> <silent> <Plug>InfoFollow :call info#FollowRef()<CR>
    nnoremap <buffer> <silent> <Plug>InfoLinkNext :call search('\(^\* .*::\\|\*note\( \\|\n\)\)')<CR>
    nnoremap <buffer> <silent> <Plug>InfoLinkPrev :call search('\(^\* .*::\\|\*note\( \\|\n\)\)', 'b')<CR>
    if !exists("g:no_plugin_maps") && !exists("g:no_info_maps")
        if !hasmapto('<Plug>InfoHeadingNext')
            nmap <buffer> ]] <Plug>InfoHeadingNext
        endif
        if !hasmapto('<Plug>InfoHeadingPrev')
            nmap <buffer> [[ <Plug>InfoHeadingPrev
        endif
        if !hasmapto('<Plug>InfoFollow')
            nmap <buffer> <C-]> <Plug>InfoFollow
        endif
        if !hasmapto('<Plug>InfoLinkNext')
            nmap <buffer> <C-I> <Plug>InfoLinkNext
        endif
        if !hasmapto('<Plug>InfoLinkPrev')
            nmap <buffer> <C-[>[Z <Plug>InfoLinkPrev
        endif
        if !hasmapto('<Plug>InfoNext')
            nmap <buffer> <LocalLeader>n <Plug>InfoNext
        endif
        if !hasmapto('<Plug>InfoPrev')
            nmap <buffer> <LocalLeader>p <Plug>InfoPrev
        endif
        if !hasmapto('<Plug>InfoUp')
            nmap <buffer> <LocalLeader>u <Plug>InfoUp
        endif
    endif
endif
