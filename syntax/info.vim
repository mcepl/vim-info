" Vim syntax file
" Language:	Man page
" Maintainer:	Jan Pobrislo <ccx@webprojekty.cz>
" Version Info:

" For version 5.x: Clear all syntax items
" For version 6.x: Quit when a syntax file was already loaded
if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

" syn case   ignore
syn match  infoTitle  /^\n.*\n\(\*\{4,}\|=\{4,}\)\n$/
syn match  infoTitle  /^\(Table\|Figure\) [0-9.]\+:\(.\+\n\)*/
syn match  infoSectionHeading  /^\n\d\+\(\.\d\+\)* .*\n\(\*\{3,}\|=\{3,}\|-\{3,}\|\.\{3,}\)$/
syn match  infoEscape  /\n.*/
syn match  infoEscape  /^File: [^,]\+,  Node:.*/
syn match  infoNavFile /^File:/   containedin=infoEscape contained conceal
syn match  infoNavNode /,  \zsNode:/ containedin=infoEscape contained conceal cchar=⇒
syn match  infoNavNext /,  \zsNext:/ containedin=infoEscape contained conceal cchar=→
syn match  infoNavPrev /,  \zsPrev:/ containedin=infoEscape contained conceal cchar=←
syn match  infoNavUp   /,  \zsUp:/   containedin=infoEscape contained conceal cchar=↑
syn match  infoMenu  /^\* Menu:/
syn match  infoReference  /^\* .*::/
syn region infoReference  start=/\*note\( \|\n\)/ end=/::\|\(Table\|Figure\)\( \|\n\)[0-9.]\+:/

setlocal cole=2 cocu=nvic

" Define the default highlighting.
" For version 5.7 and earlier: only when not done already
" For version 5.8 and later: only when an item doesn't have highlighting yet
if version >= 508 || !exists("did_info_syn_inits")
  if version < 508
    let did_info_syn_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif

  HiLink infoTitle           Title
  HiLink infoSectionHeading  Statement
  HiLink infoEscape          Constant
  HiLink infoReference       PreProc
  HiLink infoMenu            String

  delcommand HiLink
endif

let b:current_syntax = "info"

" vim:ts=8 sts=2 sw=2 et:
